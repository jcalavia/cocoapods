#
# Be sure to run `pod lib lint CoreDataUtil.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
	s.name             = "CoreDataUtil"
	s.version          = "1.0.0"
	s.summary          = "CoreData Utility methods."
	s.description      = <<-DESC
                       CoreData framework utility methods for simple usage

                       * Encapsulate creation of PersistenceStoreCoordinator
                       * Encapsulate creation of ManagedObjectContext 
                       DESC
	s.homepage         = "http://gitbucket.dev.dinsa.es/gitbucket/ios-cocoapods/core-data-util"
	s.license          = 'DINSA'
	s.author           = { 
							"Jorge Calavia Giménez" => "jcalavia@dinsa.es",
							"Daniel García Vega" => "dgarcia@dinsa.es",
						 }
	s.source           = { :git => "http://gitbucket.dev.dinsa.es/gitbucket/git/ios-cocoapods/core-data-util.git", :tag => s.version.to_s }
	
	s.platform     = :ios, '7.0'
	s.requires_arc = true
	
	s.source_files = 'Pod/Classes'
	
	s.dependency 'CocoaLumberjack'
	s.dependency 'CommonCategories/NSError'

end
