Pod::Spec.new do |s|
	s.name             = "StyledComponents"
	s.version          = "1.0.0"
	s.summary          = "iOS styled components library."
	s.description      = <<-DESC
							A CSS tuneable like components library for iOS. It reads configuration from plist files in Resources folder
							to adaptate components to fonts and colors.
						DESC
	s.homepage         = "http://gitbucket.dev.dinsa.es/gitbucket/ios-cocoapods/ios-styled-components"
	s.license          = 'DINSA'
	s.author           = { 
							"Jorge Calavia Giménez" => "jcalavia@dinsa.es" ,
							"Daniel García Vega" => "dgarcia@dinsa.es" 
						}
	s.source           = { 
							:git => "http://gitbucket.dev.dinsa.es/gitbucket/git/ios-cocoapods/styled-components.git", 
							:tag => s.version.to_s 
						}
	
	s.platform     = :ios, '7.0'
	s.requires_arc = true
	s.source_files = 'Pod/Classes'
	s.resources = 'Pod/Assets/*'

	s.subspec "UIButton" do |scbutton|
		scbutton.source_files = "Pod/Classes/UIButton"
	end

	s.subspec "UILabel" do |sclabel|
		sclabel.source_files = "Pod/Classes/UILabel"
	end

	s.subspec "UIRefreshControl" do |screfreshcontrol|
		screfreshcontrol.source_files = "Pod/Classes/UIRefreshControl"
	end

	s.subspec "UISearchBar" do |scsearchbar|
		scsearchbar.source_files = "Pod/Classes/UISearchBar"
	end

	s.subspec "UITableview" do |sctableview|
		sctableview.source_files = "Pod/Classes/UITableview/**/*"
	end

	s.subspec "UITextField" do |sctextfield|
		sctextfield.source_files = "Pod/Classes/UITextField"
	end

	s.subspec "UITextView" do |sctextview|
		sctextview.source_files = "Pod/Classes/UITextView"
	end
	
	s.dependency 'CocoaLumberjack'
	s.dependency 'CommonCategories/UIColor'

	
end
