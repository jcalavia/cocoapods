#
# Be sure to run `pod lib lint ComunicacionesCorporativas.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|

	s.name             = "ComunicacionesCorporativas"
	s.version          = "2.0.0"
	s.summary          = "MISS ComunicacionesCorporativas iOS Module."
	s.description      = <<-DESC
                       MISS ComunicacionesCorporativas iOS Module.

                       * Fetch and search chats
                       * Send and receive messages
                       * Comming soon multimedia content integration
                       DESC
	s.homepage         = "http://gitbucket.dev.dinsa.es/gitbucket/ios-cocoapods/comunicaciones-corporativas"

	s.license          = 'DINSA'
	s.author           = { 
							"Jorge Calavia Giménez" => "jcalavia@dinsa.es",
							"Daniel García Vega" => "dgarcia@dinsa.es"
						}
	s.source           = { 
							:git => "http://gitbucket.dev.dinsa.es/gitbucket/git/ios-cocoapods/comunicaciones-corporativas.git", 
							:tag => s.version.to_s 
						}

	s.platform     = :ios, '7.0'
	s.requires_arc = true
	s.source_files = 'Pod/Classes'
	s.resources = 'Pod/Assets/*.*'

	s.dependency		'DirectorioCorporativo', '~> 2.1.2'
	s.dependency        'StyledComponents', '2.1.1'
	s.dependency		'CocoaLumberjack', '~> 1.9.2' 
	s.dependency		'CommonCategories'
	s.dependency		'CoreDataUtil'
	s.dependency		'UserDefaultsUtil'
	s.dependency		'AFNetworking'

end
