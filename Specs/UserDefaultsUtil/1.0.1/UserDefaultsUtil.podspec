#
# Be sure to run `pod lib lint CoreDataUtil.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
	s.name             = "UserDefaultsUtil"
	s.version          = "1.0.1"
	s.summary          = "UserDefaults Utility methods."
	s.description      = <<-DESC
                       UserDefaults singleton utility methods for simple usage

                       * Encapsulate reading of service settings
                       * Encapsulate persstence of service settings 
                       DESC
	s.homepage         = "http://gitbucket.dev.dinsa.es/gitbucket/ios-cocoapods/user-defaults-util"
	s.license          = 'DINSA'
	s.author           = { 
							"Jorge Calavia Giménez" => "jcalavia@dinsa.es",
							"Daniel García Vega" => "dgarcia@dinsa.es",
						 }
	s.source           = { :git => "http://gitbucket.dev.dinsa.es/gitbucket/git/ios-cocoapods/user-defaults-util.git", :tag => s.version.to_s }
	
	s.platform     = :ios, '7.0'
	s.requires_arc = true
	
	s.source_files = 'Pod/Classes'
	
	s.dependency		'CocoaLumberjack', '~> 1.9.2' 

end
