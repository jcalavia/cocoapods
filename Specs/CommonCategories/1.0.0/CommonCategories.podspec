#
# Be sure to run `pod lib lint CommonCategories.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = "CommonCategories"
    s.version          = "1.0.0"
    s.summary          = "Commonly used categories."
    
    s.description      = <<-DESC
                        Set of commonly used categories including:
                        
                            * NSDate methods to convert dates from / to milliseconds and ISO
                            * NSError methods to perform common conversions
                            * UIColor convenience methods to build colors based on HEX values
                        DESC
    
    s.homepage         = "http://gitbucket.dev.dinsa.es/gitbucket/ios-cocoapods/common-categories"
    s.license          = 'DINSA'
    s.author           = { 
                            "Jorge Calavia Giménez" => "jcalaviadinsa.es",
                            "Daniel García Vega" => "dgarcia@dinsa.es"
                        }
    
    s.source           = { :git => "http://gitbucket.dev.dinsa.es/gitbucket/git/ios-cocoapods/common-categories.git", :tag => s.version.to_s }

	s.platform     = :ios, '7.0'
	s.ios.deployment_target = '7.0'
	s.requires_arc = true
	
	s.dependency 'CocoaLumberjack'

	s.subspec "NSDate" do |date|
		date.public_header_files = 'Classes/NSDate/*.h'
		date.source_files = "Pod/Classes/NSDate"
	end

	s.subspec "NSError" do |error|
		error.dependency 'AFNetworking'
        error.public_header_files = 'Classes/NSError/*.h'
		error.source_files = "Pod/Classes/NSError"
	end

	s.subspec "UIColor" do |color|
		color.public_header_files = 'Classes/UIColor/*.h'
		color.source_files = "Pod/Classes/UIColor"
	end

end
