
Pod::Spec.new do |s|
  s.name             = "Mensajeria"
  s.version          = "1.0.0"
  s.summary          = "Modular Adaptable Simple Suite iOS corporative directory."
  s.description      = <<-DESC
  						A CSS tuneable like components library for iOS. It reads configuration from plist files in Resources folder
  						to adaptate components to fonts and colors.
                       DESC
  s.homepage         = "https://bitbucket.org/jcalavia/mensajeria-instantanea"
  s.screenshots      = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Daniel Garcia Vega" => "dgarcia@dinsa.es" }
  s.source           = { :git => "https://bitbucket.org/jcalavia/mensajeria-instantanea.git", :tag => "#{s.version}" }
  s.social_media_url = 'https://twitter.com/NAME'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.resources = "chat-ios-client/src/xcode/Resources/*.*"
  s.public_header_files = 'chat-ios-client/src/xcode/Mensajeria/**/*.h'

  s.dependency		'MASSStyledComponents'
  s.dependency		'AFNetworking'
  s.dependency		'MBProgressHUD'
  s.dependency		'DirectorioCorporativo'

  s.subspec 'Core' do |core|
    core.source_files = 'chat-ios-client/src/xcode/Mensajeria/**/*.{h,m}'
  end
  
end