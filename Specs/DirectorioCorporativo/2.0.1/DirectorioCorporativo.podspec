#
# Be sure to run `pod lib lint DirectorioCorporativo.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
	s.name             = "DirectorioCorporativo"
	s.version          = "2.0.1"
	s.summary          = "MISS DirectorioCorporativo iOS Module."
	s.description      = <<-DESC
                       MISS DirectorioCorporativo iOS Module.

                       * Fetch and search contacts
                       * Fetch and search groups
                       * Comming soon edit personal data
                       DESC
	s.homepage         = "http://gitbucket.dev.dinsa.es/gitbucket/ios-cocoapods/directorio-corporativo"
	s.license          = 'DINSA'
	s.author           = { 
							"Jorge Calavia Giménez" => "jcalavia@dinsa.es",
							"Daniel García Vega" => "dgarcia@dinsa.es"
						}
	s.source           = { 
							:git => "http://gitbucket.dev.dinsa.es/gitbucket/git/ios-cocoapods/directorio-corporativo.git", 
							:tag => s.version.to_s 
						}
						
	s.platform     = :ios, '7.0'
	s.requires_arc = true
	s.source_files = 'Pod/Classes'
	s.resources = 'Pod/Assets/*.*'

	s.dependency		'CocoaLumberjack', '~> 1.9.2' 
	s.dependency		'CommonCategories'
	s.dependency		'StyledComponents', '~> 2.0.0' 
	s.dependency		'CoreDataUtil'
	s.dependency		'UserDefaultsUtil'
	s.dependency		'AFNetworking'
	s.dependency		'MBProgressHUD'
	
end
